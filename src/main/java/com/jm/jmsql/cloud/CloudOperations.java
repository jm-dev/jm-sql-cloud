/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.cloud;

import com.jm.jmsql.cloud.entities.Databases;
import com.jm.jmsql.cloud.rs.Message;
import com.jm.jmsql.cloud.rs.Result;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author Michael L.R. Marques
 */
@Path("/cloud-operations")
public class CloudOperations {
    
    /**
     * 
     * @param username
     * @return 
     */
    @GET
    @Produces("application/xml")
    @Path("get-db-xplora/{username}")
    public Databases getDBXplora(@PathParam("username") String username) {
//        User user = UserMaintenance.getInstance().getUserByUsername(username);
//        
//        return DBXPloraMaintenance.getInstance().getDatabases(user);
        return null;
    }
    
    /**
     * 
     * @param username
     * @param databases
     * @return 
     */
    @GET
    @Produces("application/xml")
    @Path("set-db-xplora/{username}/{databases}")
    public Result setDBXplora(@PathParam("username") String username, @PathParam("databases") Databases databases) {
//        User user = UserMaintenance.getInstance().getUserByUsername(username);
//        if (DBXPloraMaintenance.getInstance().setDatabases(user, databases)) {
//            return new Result();
//        }
        return new Result(false, new Message("There was an error updating the DBXPlora"));
    }
    
}
