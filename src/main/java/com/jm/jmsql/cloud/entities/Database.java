/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.cloud.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Michael L.R. Marques
 */
@Entity
@Table(name = "databases")
@XmlRootElement()
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Database implements Serializable {
    
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String className;
    private String artefact;
    private String libraryDelimeter;
    private boolean uppercasePrefered;
    private int userID;
    @Transient
    private List<Definition> definitions;
    
    /**
     * 
     */
    public Database() {
        this(new ArrayList());
    }
    
    /**
     * 
     * @param definitions 
     */
    public Database(List<Definition> definitions) {
        super();
        this.definitions = definitions;
    }

    /**
     * @return the id
     */
    @XmlTransient
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    @XmlElement
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the className
     */
    @XmlElement
    public String getClassName() {
        return className;
    }

    /**
     * @param className the className to set
     */
    public void setClassName(String className) {
        this.className = className;
    }

    /**
     * @return the artefact
     */
    @XmlElement
    public String getArtefact() {
        return artefact;
    }

    /**
     * @param artefact the artefact to set
     */
    public void setArtefact(String artefact) {
        this.artefact = artefact;
    }
    
    /**
     * @return the libraryDelimeter
     */
    @XmlAttribute
    public String getLibraryDelimeter() {
        return libraryDelimeter;
    }

    /**
     * @param libraryDelimeter the libraryDelimeter to set
     */
    public void setLibraryDelimeter(String libraryDelimeter) {
        this.libraryDelimeter = libraryDelimeter;
    }

    /**
     * @return the uppercasePrefered
     */
    @XmlAttribute
    public boolean isUppercasePrefered() {
        return uppercasePrefered;
    }

    /**
     * @param uppercasePrefered the uppercasePrefered to set
     */
    public void setUppercasePrefered(boolean uppercasePrefered) {
        this.uppercasePrefered = uppercasePrefered;
    }

    /**
     * @return the definitions
     */
    @XmlElement
    public List<Definition> getDefinitions() {
        return definitions;
    }
    
    /**
     * @param definitions the definitions to set
     */
    public void setDefinitions(List<Definition> definitions) {
        this.definitions = definitions;
    }
    
    /**
     * @return the userID
     */
    @XmlTransient
    public int getUserID() {
        return userID;
    }

    /**
     * @param userID the userID to set
     */
    public void setUserID(int userID) {
        this.userID = userID;
    }
    
}
