/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.cloud.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Michael L.R. Marques
 */

@XmlRootElement()
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Databases implements Serializable {
    
    private List<Database> databases;
    
    /**
     * 
     */
    public Databases() {
       this(new ArrayList());
    }
    
    /**
     * 
     * @param databases 
     */
    public Databases(List<Database> databases) {
       super();
       this.databases = databases;
    }

    /**
     * @return the databases
     */
    @XmlElement(name = "database")
    public List<Database> getDatabases() {
        return databases;
    }

    /**
     * @param databases the databases to set
     */
    public void setDatabases(List<Database> databases) {
        this.databases = databases;
    }
    
}
