package com.jm.jmsql.cloud.entities;
// Generated 03 Sep 2014 12:11:01 AM by Hibernate Tools 3.6.0

import com.jm.jmsql.cloud.validation.ValidationParameters;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author Michael L.R. Marques
 */
@Entity
@Table(name = "users")
public class User implements ValidationParameters, Serializable {
    
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String username;
    private String password;
    private String email;
    private Date creationDate;
    private Date creationTime;
    private Date loginDate;
    private Date loginTime;
    private Boolean verified;
    
    /**
     * 
     */
    public User() {
        super();
    }
    
    /**
     * 
     * @param username
     * @param password
     * @param email 
     */
    public User(String username, String password, String email) {
        this.username = username;
        this.password = password;
        this.email = email;
    }
    
    /**
     * 
     * @param id
     * @param username
     * @param password
     * @param email
     * @param creationDate
     * @param creationTime
     * @param loginDate
     * @param loginTime 
     */
    public User(int id, String username, String password, String email, Date creationDate, Date creationTime, Date loginDate, Date loginTime) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.creationDate = creationDate;
        this.creationTime = creationTime;
        this.loginDate = loginDate;
        this.loginTime = loginTime;
    }
    
    /**
     * 
     * @param id
     * @param username
     * @param password
     * @param email
     * @param creationDate
     * @param creationTime
     * @param loginDate
     * @param loginTime
     * @param verified 
     */
    public User(int id, String username, String password, String email, Date creationDate, Date creationTime, Date loginDate, Date loginTime, Boolean verified) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.creationDate = creationDate;
        this.creationTime = creationTime;
        this.loginDate = loginDate;
        this.loginTime = loginTime;
        this.verified = verified;
    }

    public int getID() {
        return this.id;
    }

    public void setID(int id) {
        this.id = id;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getCreationTime() {
        return this.creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public Date getLoginDate() {
        return this.loginDate;
    }

    public void setLoginDate(Date loginDate) {
        this.loginDate = loginDate;
    }

    public Date getLoginTime() {
        return this.loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    public Boolean getVerified() {
        return this.verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }

}
