/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.cloud.persitence;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Michael L.R. Marques
 * @param <T>
 */
public class PersistenceService<T extends Serializable> {
    
    private static EntityManagerFactory entityManagerFactory;
    
    /**
     * 
     */
    public PersistenceService() {
        if (entityManagerFactory == null) {
            entityManagerFactory = Persistence.createEntityManagerFactory("transactions-optional");
        }
    }
    
    /**
     * 
     * @return 
     */
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }
    
    /**
     *
     * @param entity
     */
    public void add(T entity) {
        EntityManager manager = getEntityManager();
        manager.persist(entity);
    }
    
    /**
     * 
     * @param type
     * @param value
     * @return 
     */
    public T get(Class<T> type, String value) {
        return getEntityManager().find(type, value);
    }
    
    /**
     * 
     * @param query
     * @return 
     */
    public List<T> getList(Query query) {
        return new ArrayList<>();
    }
    
}
