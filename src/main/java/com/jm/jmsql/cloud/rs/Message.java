/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jm.jmsql.cloud.rs;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Marques
 */
@XmlRootElement(name = "message")
public class Message {
    
    private Type type;
    private String text;
    
    /**
     * 
     */
    public Message() {
        super();
    }
    
    /**
     * 
     * @param text 
     */
    public Message(String text) {
        this(Type.INFO, text);
    }
    
    /**
     * 
     * @param type
     * @param text 
     */
    public Message(Type type, String text) {
        this.type = type;
        this.text = text;
    }

    /**
     * @return the type
     */
    public Type getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(Type type) {
        this.type = type;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }
    
    /**
     * 
     *  @author Marques
     */
    public enum Type {
        
        INFO,
        VALIDATION,
        ERROR,
        CRITICAL,
        WARNING
        
    }
    
}
