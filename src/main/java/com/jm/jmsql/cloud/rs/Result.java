/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jm.jmsql.cloud.rs;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Marques
 */
@XmlRootElement(name = "result")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Result implements List<Message>, Serializable {
    
    private boolean successful;
    private List<Message> messages;
    
    /**
     * 
     */
    public Result() {
        this(true, new ArrayList());
    }
    
    /**
     * 
     * @param message 
     */
    public Result(String message) {
        this(true, new Message(message));
    }
    
    /**
     * 
     * @param successful
     * @param messages 
     */
    public Result(boolean successful, Message... messages) {
        this(successful, Arrays.asList(messages));
    }
    
    /**
     * 
     * @param successful
     * @param messages 
     */
    public Result(boolean successful, List<Message> messages) {        
        this.successful = successful;
        this.messages = messages;
    }

    /**
     * @return the successful
     */
    @XmlElement(name = "successful")
    public boolean isSuccessful() {
        return this.successful;
    }

    /**
     * @param successful the successful to set
     */
    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }

    /**
     * @return the message
     */
    @XmlElement(name = "message")
    public List<Message> getMessages() {
        return this.messages;
    }
    
    /**
     * 
     * @param messages 
     */
    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public int size() {
        return this.messages.size();
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public boolean isEmpty() {
        return this.messages.isEmpty();
    }
    
    /**
     * 
     * @param o
     * @return 
     */
    @Override
    public boolean contains(Object o) {
        return this.messages.contains(o);
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public Iterator<Message> iterator() {
        return this.messages.iterator();
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public Object[] toArray() {
        return this.messages.toArray();
    }
    
    /**
     * 
     * @param <T>
     * @param a
     * @return 
     */
    @Override
    public <T> T[] toArray(T[] a) {
        return this.messages.toArray(a);
    }
    
    /**
     * 
     * @param e
     * @return 
     */
    @Override
    public boolean add(Message e) {
        return this.messages.add(e);
    }
    
    /**
     * 
     * @param o
     * @return 
     */
    @Override
    public boolean remove(Object o) {
        return this.messages.remove(o);
    }
    
    /**
     * 
     * @param c
     * @return 
     */
    @Override
    public boolean containsAll(Collection<?> c) {
        return this.messages.containsAll(c);
    }
    
    /**
     * 
     * @param c
     * @return 
     */
    @Override
    public boolean addAll(Collection<? extends Message> c) {
        return this.messages.addAll(c);
    }
    
    /**
     * 
     * @param index
     * @param c
     * @return 
     */
    @Override
    public boolean addAll(int index, Collection<? extends Message> c) {
        return this.messages.addAll(index, c);
    }
    
    /**
     * 
     * @param c
     * @return 
     */
    @Override
    public boolean removeAll(Collection<?> c) {
        return this.messages.removeAll(c);
    }
    
    /**
     * 
     * @param c
     * @return 
     */
    @Override
    public boolean retainAll(Collection<?> c) {
        return this.messages.retainAll(c);
    }
    
    /**
     * 
     */
    @Override
    public void clear() {
        this.messages.clear();
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public Message get(int index) {
        return this.messages.get(index);
    }
    
    /**
     * 
     * @param index
     * @param element
     * @return 
     */
    @Override
    public Message set(int index, Message element) {
        return this.messages.set(index, element);
    }
    
    /**
     * 
     * @param index
     * @param element 
     */
    @Override
    public void add(int index, Message element) {
        this.messages.add(index, element);
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public Message remove(int index) {
        return this.messages.remove(index);
    }
    
    /**
     * 
     * @param o
     * @return 
     */
    @Override
    public int indexOf(Object o) {
        return this.messages.indexOf(o);
    }
    
    /**
     * 
     * @param o
     * @return 
     */
    @Override
    public int lastIndexOf(Object o) {
        return this.messages.lastIndexOf(o);
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public ListIterator<Message> listIterator() {
        return this.messages.listIterator();
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public ListIterator<Message> listIterator(int index) {
        return this.messages.listIterator(index);
    }
    
    /**
     * 
     * @param fromIndex
     * @param toIndex
     * @return 
     */
    @Override
    public List<Message> subList(int fromIndex, int toIndex) {
        return this.messages.subList(fromIndex, toIndex);
    }
    
}
