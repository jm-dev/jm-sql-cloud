/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.cloud.util;

import static com.jm.jmsql.cloud.validation.ValidationService.EMAIL_ADDRESS;

/**
 *
 * @author Michael L.R. Marques
 */
public class Validation {


    /**
     * 
     * @param username
     * @return
     */
    public static boolean isUsingEmailAddress(String username) {
        return username.matches(EMAIL_ADDRESS);
    }

}
