/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.cloud.validation;

import com.jm.jmsql.cloud.entities.User;
import com.jm.jmsql.cloud.rs.Message;
import com.jm.jmsql.cloud.rs.Message.Type;
import com.jm.jmsql.cloud.rs.Result;
import static com.jm.jmsql.cloud.validation.ValidationService.EMAIL_DOES_NOT_EXIST;
import static com.jm.jmsql.cloud.validation.ValidationService.INCORRECT_PASSWORD;
import static com.jm.jmsql.cloud.validation.ValidationService.USERNAME_DOES_NOT_EXIST;

/**
 *
 * @author Michael L.R. Marques
 */
public class LoginValidation implements ValidationService<User> {
    
    private final boolean usingEmail;
    private final String username;
    private final String emailAddress;
    private final String password;
    
    /**
     * 
     * @param usingEmail
     * @param user
     * @param password 
     */
    public LoginValidation(boolean usingEmail, String user, String password) {
        this.usingEmail = usingEmail;
        this.username = user;
        this.emailAddress = user;
        this.password = password;
    }

    /**
     * @return the usingEmail
     */
    public boolean isUsingEmail() {
        return usingEmail;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @return the emailAddress
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }
    
    /**
     * 
     * @param user
     * @return 
     */
    @Override
    public Result performValidation(User user) {
        if (user == null) {
            return new Result(false, new Message(Type.VALIDATION, isUsingEmail() ? EMAIL_DOES_NOT_EXIST : USERNAME_DOES_NOT_EXIST));
        } else if (isUsingEmail() && user.getEmail().matches(EMAIL_ADDRESS)) {
            return new Result(false, new Message(Type.VALIDATION, "That is not a valid email address"));
//        } else if (!isUsingEmail() && user.getUsername().matches(USERANAME)) {
        } else if (!user.getPassword().equals(getPassword())) {
            return new Result(false, new Message(Type.VALIDATION, INCORRECT_PASSWORD));
        }
        return new Result("Login successful!");
    }
    
}
