/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jm.jmsql.cloud.validation;

import com.jm.jmsql.cloud.rs.Message;
import com.jm.jmsql.cloud.rs.Message.Type;
import com.jm.jmsql.cloud.rs.Result;

/**
 *
 * @author Michael L.R. Marques
 */
public class RegisterValidation implements ValidationService<RegisterParameters> {
    
    /**
     * 
     * @param params
     * @return 
     */
    @Override
    public Result performValidation(RegisterParameters params) {
        Result result = new Result();
        
        if (params.getUsername() == null ||
                params.getUsername().isEmpty()) {
            result.setSuccessful(false);
            result.add(new Message(Type.VALIDATION, USERNAME_EMPTY));
        }
        if (!params.getPassword().matches(PASSWORD)) {
            result.setSuccessful(false);
            result.add(new Message(Type.ERROR, INVALID_PASSWORD));
            result.add(new Message(Type.VALIDATION, "Password length of 8 to 32 characters."));
            result.add(new Message(Type.VALIDATION, "A digit must occur at least once."));
            result.add(new Message(Type.VALIDATION, "A lower case letter must occur at least once."));
            result.add(new Message(Type.VALIDATION, "An upper case letter must occur at least once."));
            result.add(new Message(Type.VALIDATION, "No whitespaces allowed."));
        }
        if (!params.getEmail().matches(EMAIL_ADDRESS)) {
            result.setSuccessful(false);
            result.add(new Message(Type.ERROR, INVALID_EMAIL_ADDRESS));
            result.add(new Message(Type.VALIDATION, "E.G: smiths.johnson@domainian.com"));
        }
        
        if (result.isSuccessful()) {
            result.add(new Message("Registration was successful!"));
        }
        
        return result;
    }
    
}
