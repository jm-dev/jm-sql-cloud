/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.cloud.validation;

import com.jm.jmsql.cloud.rs.Result;

/**
 *
 * @author Michael L.R. Marques
 * @param <T>
 */
public interface ValidationService<T extends ValidationParameters> {
    
    // Match an email address
    static final String EMAIL_ADDRESS = "^[-._A-Za-z0-9]+(\\\\\\\\\\\\\\\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\\\\\\\\\\\\\\\.[A-Za-z0-9-]+)*(.[A-Za-z]{2,3})*(.[A-Za-z]{2,3})$";
    // Match a password
    static final String PASSWORD = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,32}$";
    /**
     * Constants
     */
    static final String INCORRECT_PASSWORD = "Incorrect password!";
    static final String USERNAME_DOES_NOT_EXIST = "Username provided does not exist!";
    static final String EMAIL_DOES_NOT_EXIST = "Email address provided does not exist!";
    static final String USERNAME_EMPTY = "Username cannot be empty!";
    static final String INVALID_EMAIL_ADDRESS = "Not a valid email address!";
    static final String INVALID_PASSWORD = "Not a valid password!";
    
    /**
     * 
     * @param params
     * @return ValidationResult
     */
    public Result performValidation(T parameter);
    
}
