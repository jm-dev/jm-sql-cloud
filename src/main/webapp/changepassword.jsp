<%-- 
    Document   : changepassword
    Created on : 04 Sep 2014, 11:51:34 PM
    Author     : Marques
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" import="java.util.*" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Change Password</title>
        <script>
            
            function doChangePasswordAction() {
                document.getElementById('frmChangePassword').submit();
            }
            
        </script>
    </head>
    <body>
        <form method="GET" action="frmChangePassword">
            <table>
                <thead>
                    <td>Change Password:</td>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <input type="text" name="txtPassword" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="submit" name="btnSubmit" value="Submit" action="doChangePasswordAction()"/>
                        </td>
                    </tr>
                </tbody>    
            </table>
        </form>
    </body>
</html>
